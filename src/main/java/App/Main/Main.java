package App.Main;


import App.config.ConfigApp;
import App.controller.TaskController;
import App.controller.UserController;
import App.model.User;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(ConfigApp.class);
        context.refresh();

        UserController userController = context.getBean(UserController.class);
        TaskController taskController = context.getBean(TaskController.class);

        User testUser = User.builder().userId((long) 1)
                .firstName("James")
                .lastName("Bond")
                .email("bond@aol.com")
                .password("333").build();
        userController.signUp(testUser);

        System.out.println(userController.signIn(testUser));


        //        userController.signUp(testUser);
//        userController.signIn(testUser);

    }

}


