package App.Repository;

import App.model.Task;

import java.util.Set;

public interface TaskRepository {

    Task createTask(Long userId, Task task);

    void deleteTask(Long taskId);

    Set<Task> findAllTasksByUser(Long userId);

//    void setStatus(Long taskId, TaskStatus taskStatus);
}

