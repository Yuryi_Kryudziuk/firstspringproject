package App.Repository;

import App.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    HashMap<Long, User> userHashMap;
// Long UserId;

    @Autowired
    @Qualifier("userHashMap")
    public void setUserHashMap(HashMap<Long, User> userHashMap) {
        this.userHashMap = userHashMap;
    }


//    @Autowired
//    @Qualifier("UserId")
//    public void setUserId(Long userId) {
//        UserId = userId;
//    }

    @Override
    public boolean isExistById(Long userId) {
        boolean flag = false;
        if (!userHashMap.isEmpty()) {
            for (Map.Entry<Long, User> pair : userHashMap.entrySet()) {
                flag = pair.getKey().equals(userId);
            }
        }
        return flag;

    }


    @Override
    public void createUser(User user) {
        userHashMap.put(user.getUserId(), user);
    }

    @Override
    public Optional<Long> findUserIdByEmailAndPassword(String email, String password) {
        Optional<Long> UserId = Optional.empty();
        for (Map.Entry<Long, User> pair : userHashMap.entrySet()) {
            if (pair.getValue().getEmail().equals(email) || pair.getValue().getPassword().equals(password)) {
                UserId = Optional.ofNullable(pair.getKey());
            } else {
                Optional.empty();
            }
        }
        return UserId;
    }
}
