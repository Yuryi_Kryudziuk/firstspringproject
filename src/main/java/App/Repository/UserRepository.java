package App.Repository;

import App.model.User;

import java.util.Optional;

public interface UserRepository {


    boolean isExistById(Long UserId);


    void createUser(User user);

    Optional<Long> findUserIdByEmailAndPassword(String email, String password);
}
