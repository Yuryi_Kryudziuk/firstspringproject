package App.Repository;

import App.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    HashMap<Long, Task> taskHashMap;

    @Autowired
    @Qualifier("taskHashMap")
    public void setTaskHashMap(HashMap<Long, Task> taskHashMap) {
        this.taskHashMap = taskHashMap;
    }

    @Override
    public Task createTask(Long taskId, Task task) {
        return taskHashMap.put(taskId, task);
    }

    @Override
    public void deleteTask(Long taskId) {
        for (Map.Entry<Long, Task> pair : taskHashMap.entrySet()) {
            if (pair.getKey().equals(taskId)) {
                taskHashMap.remove(taskId);
            }
        }
    }

    @Override
    public Set<Task> findAllTasksByUser(Long userId) {
        Set<Task> setTask = new LinkedHashSet<>();

        for (Map.Entry<Long, Task> pair : taskHashMap.entrySet()) {
            if (pair.getValue().getUserId().equals(userId))
                setTask.add(pair.getValue());
        }
        return setTask;
    }
}

//    @Override
//    public void setStatus(Long taskId, TaskStatus taskStatus) {
//
//    }
//}
