package App.service;

import App.Repository.UserRepositoryImpl;
import lombok.RequiredArgsConstructor;
import App.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    @Autowired
    private final UserRepositoryImpl userRepositoryImpl;

    @Override
    public boolean signUp(User user) {
        if (userRepositoryImpl.isExistById(user.getUserId())) {
            return false;
        } else {
            userRepositoryImpl.createUser(user);
            return true;
        }
    }

    @Override
    public Optional<Long> signIn(User user) {

        return userRepositoryImpl.findUserIdByEmailAndPassword(user.getEmail(), user.getPassword());
    }
}
