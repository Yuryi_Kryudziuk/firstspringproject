package App.service;

import App.model.User;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserService {
    boolean signUp(User user);

    Optional<Long> signIn(User user);

}
