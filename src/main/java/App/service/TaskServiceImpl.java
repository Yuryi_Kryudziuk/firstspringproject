package App.service;

import App.Repository.TaskRepository;
import App.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }
    @Override
    public void createTask(Long taskId, Task task) {
        taskRepository.createTask(taskId, task);
    }

    @Override
    public void deleteTask(Long taskId) {
        taskRepository.deleteTask(taskId);
    }

    @Override
    public Set<Task> findAllUserTasks(Long userId) {
        return taskRepository.findAllTasksByUser(userId);
    }
}

//    @Override
//    public void closeTask(Long taskId) {
//        taskRepositoryImpl.setStatus(taskId, TaskStatus.CLOSED);
//    }
//
//    @Override
//    public void openTask(Long taskId) {
//        taskRepositoryImpl.setStatus(taskId, TaskStatus.OPEN);
//    }
//}
