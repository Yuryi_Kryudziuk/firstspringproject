package App.controller;

import App.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import App.service.AuthService;
import App.service.UserService;

import java.util.Optional;

@Component
public class UserController {
    private final UserService userService;
    private final AuthService authService;

    @Autowired
    public UserController(UserService userService, AuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    public boolean signUp(User user) {
        return userService.signUp(user);
    }

    public Optional<Long> signIn(User user) {
        return userService.signIn(user);
    }
}
